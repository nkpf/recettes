AUTHOR = 'blopouet'
SITENAME = 'Recettes'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = 'fr'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Theme

THEME = "./themes/elegant"
LANDING_PAGE_TITLE = "Compilation de recettes"

# Blogroll
LINKS = ()

DEFAULT_PAGINATION = 10

STATIC_PATHS = ['images', 'theme/images']
USE_SHORTCUT_ICONS = True

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

AUTHORS = {
    "Morgane": {
        "blurb": "un blop",
    },
    "Nicolas": {
        "blurb": "un pouet",
    },

}
