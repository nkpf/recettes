Fleurs de courgettes fries
==========================

:date: 2022-11-02 19:21
:modified: 2022-11-03 21:33
:category: plat
:slug: fleurs-de-courgettes
:author: Morgane
:summary: des fleurs de courgettes fries !
          
Ingrédients
-----------

Pour la pâte:

- 150g de farine
- 2 oeufs
- 25cl de lait
- 2 cuillers d’huile d’olive
- sel
- ail
- persil

Étapes
------

1. Mélanger le tout.
   
.. tip::
   
   Il est possible de monter les œufs en neige

2. Tremper les fleurs dans la pâte et bien les enrober.
3. Dans une poêle, mettre de l'huile à chauffer
4. Puis faites les cuire et les égoutter sur un sopalin
